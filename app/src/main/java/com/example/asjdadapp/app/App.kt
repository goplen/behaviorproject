package com.example.asjdadapp.app

import android.app.Application
import com.example.asjdadapp.di.repoModule
import com.example.asjdadapp.di.viewModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application(){

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(repoModule, viewModule)
        }
    }
}