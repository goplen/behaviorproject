package com.example.asjdadapp.data.repository

import com.example.asjdadapp.data.storage.CheckConnectionStorageRepository
import com.example.asjdadapp.domain.repository.CheckConnectionRepository

class CheckConnectionRepositoryImpl(private val checkConnectionStorageRepository: CheckConnectionStorageRepository): CheckConnectionRepository {
    override fun checkConnection(): Boolean {
        return checkConnectionStorageRepository.checkConnection()
    }
}