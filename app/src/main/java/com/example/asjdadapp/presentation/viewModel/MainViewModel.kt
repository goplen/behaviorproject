package com.example.asjdadapp.presentation.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.asjdadapp.domain.useCase.CheckConnectionUseCase

class MainViewModel(
    private val checkConnectionUseCase: CheckConnectionUseCase
): ViewModel() {

    val connect = MutableLiveData<Boolean>()

    fun checkConnection() {
        connect.value = checkConnectionUseCase.execute()
    }
}