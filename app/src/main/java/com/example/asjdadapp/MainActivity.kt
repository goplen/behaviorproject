package com.example.asjdadapp

import android.R
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.asjdadapp.databinding.ActivityMainBinding
import com.example.asjdadapp.databinding.BottomSheetBinding
import com.example.asjdadapp.presentation.BlankFragment
import com.example.asjdadapp.presentation.viewModel.MainViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {
    private val vm: MainViewModel by viewModel()
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        vm.checkConnection()
        initObserver()
        supportFragmentManager.beginTransaction()
            .replace(binding.fragmentContainerView.id, BlankFragment())

// настройка поведения нижнего экрана
        val bottomSheetBehavior = BottomSheetBehavior.from(binding.adsa.bottomSheet)
        bottomSheetBehavior.isDraggable=true

// настройка состояний нижнего экрана

// настройка состояний нижнего экрана
        //bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        //bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_SETTLING
        //bottomSheetBehavior.state = BottomSheetBehavior.SAVE_HIDEABLE

// настройка максимальной высоты
//        bottomSheetBehavior.peekHeight = 340

// настройка возможности скрыть элемент при свайпе вниз

// настройка колбэков при изменениях
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                Log.d("slideoffsets", "$newState")
            }
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                Log.d("slideoffset", "$slideOffset")
            }
        })
    }

    private fun initObserver() {
        vm.connect.observe(this) {
            Toast.makeText(this, "$it", Toast.LENGTH_SHORT).show()
        }
    }
}